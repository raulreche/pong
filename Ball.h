#ifndef BALL_H
#define BALL_H

#include "includes.h"
#include "ofMain.h"

class ball
{
	public:
		ball();
		ball(int x, int y, int w, int h);
		~ball();

		void update();
		void render();

		//Setter and Getter
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();

		void setSpeedX(int speed_x);
		void setSpeedY(int speed_y);
		void SpeedX_augment();
		void SpeedY_augment();
		void invertSpdX();
		void invertSpdY();
		int getSpeedX();
		int getSpeedY();
		void set_initial_speed();
		void speed_augment_coll_paddle();//aun por ver

		void setColor(ofColor colour);

	protected:
	
	private:
		C_Rectangle mpRect; //Pad Rect
		int mpSpeedX;		//Ball Speed in X
		int mpSpeedY;		//Ball Speed in Y
		ofColor mpColour;	//Pad Colour

};

#endif
