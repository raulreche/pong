#ifndef PADDLE_H
#define PADDLE_H

#include "includes.h"
#include "ofMain.h"

class Paddle
{
	public:
		Paddle();
		Paddle(int x, int y, int w, int h);
		~Paddle();

		void update();
		void render();

		//Setter and Getter
		void setRect(C_Rectangle a_rect);
		void setXY(int x, int y);
		void setX(int x);
		void setY(int y);
		void setW(int w);
		void setH(int h);

		C_Rectangle getRect();
		int getX();
		int getY();
		int getW();
		int getH();

		void setSpeed(int speed);
		int getSpeed();

		void setColor(ofColor colour);

	protected:
	
	private:
		C_Rectangle mpRect; //Pad Rect
		int mpSpeed;		//Pad Speed
		ofColor mpColour;	//Pad Colour

};

#endif
