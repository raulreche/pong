//Include our classes
#include "ball.h"

ball::ball(){
	mpRect.x = 0;
	mpRect.y = 0;
	mpRect.w = 0;
	mpRect.h = 0;
	mpSpeedX = 0;
	mpSpeedY = 0;
	mpColour = ofColor(255, 255, 255);
}

ball::ball(int x, int y, int w, int h) {
	ball();
	setXY(x, y);
	setW(w);
	setH(h);
}

ball::~ball(){
}

void ball::update(){	
	mpRect.x = mpRect.x + mpSpeedX*(int)global_delta_time / 1000; //Moviment Rectilini Uniforme
	mpRect.y = mpRect.y + mpSpeedY*(int)global_delta_time/1000; //Moviment Rectilini Uniforme
	return;
}


void ball::render(){
	ofSetColor(mpColour);
	ofDrawCircle(ofPoint(mpRect.x + mpRect.w / 2, mpRect.y + mpRect.h / 2), mpRect.w/2); //la formula del circulo, la hitbox es de un cuadrado pero visualmente es una bola
	//ofDrawRectangle(mpRect.x, mpRect.y, mpRect.w, mpRect.h);
	return;
}

//Setter and Getter
void ball::setRect(C_Rectangle a_rect) {
	setXY(a_rect.x, a_rect.y);
	setW(a_rect.w);
	setH(a_rect.h);
	return;
}

void ball::set_initial_speed() {
	setSpeedX(200);
	setSpeedY(200);
}

void ball::speed_augment_coll_paddle() {//aun por ver todo esto es caca
	SpeedX_augment();
	SpeedY_augment();
}

void ball::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void ball::setX(int x) {
	mpRect.x = x;
	return;
}

void ball::setY(int y) {
	mpRect.y = y;
	return;
}

void ball::setW(int w) {
	mpRect.w = w;
	return;
}

void ball::setH(int h) {
	mpRect.h = h;
	return;
}

C_Rectangle ball::getRect() {
	return mpRect;
}

int ball::getX() {
	return mpRect.x;
}

int ball::getY() {
	return mpRect.y;
}

int ball::getW() {
	return mpRect.w;
}

int ball::getH() {
	return mpRect.h;
}

void ball::invertSpdX(){
	mpSpeedX = -mpSpeedX;
	return;
}

void ball::invertSpdY() {
	mpSpeedY = -mpSpeedY;
	return;
}

void ball::setSpeedX(int speed_x) {
	mpSpeedX = speed_x;
	return;
}

void ball::setSpeedY(int speed_y) {
	mpSpeedY = speed_y;
	return;
}

void ball::SpeedX_augment() {
	mpSpeedX = (mpSpeedX * 1200)/1000;
	if (mpSpeedX > 300) mpSpeedX = 300;
	return;
}

void ball::SpeedY_augment() {
	mpSpeedY = (mpSpeedY *1200)/1000;
	if (mpSpeedY > 300) mpSpeedY = 300;
	return;
}

int ball::getSpeedX() {
	return mpSpeedX;
}

int ball::getSpeedY() {
	return mpSpeedY;
}

void ball::setColor(ofColor colour) {
	mpColour = colour;
	return;
}
