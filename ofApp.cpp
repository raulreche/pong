#include "ofApp.h"
#include <ctime>
#include "includes.h"
#include "Utils.h"

const clock_t begin_time = clock();
clock_t old_time;
clock_t new_time;

unsigned int global_delta_time;

bool key_down[255];

//--------------------------------------------------------------
void ofApp::setup(){ //Begins
	for (int i = 0; i < 255; i++) {
		key_down[i] = false;
	}
	
	ofSetFrameRate(60); //Max FPS Rate
	old_time = begin_time;
	new_time = begin_time;
	
	int ball_size = 40;

	mpPaddle1 = new Paddle(20, SCREEN_HEIGHT / 2 - 50, 20, 100);
	mpPaddle1->setSpeed(0);

	mpPaddle2 = new Paddle(SCREEN_WIDTH-20-20, SCREEN_HEIGHT / 2 - 50, 20, 100);
	mpPaddle2->setSpeed(0);

	mpBall = new ball	(	(SCREEN_WIDTH - ball_size) / 2,
							(SCREEN_HEIGHT - ball_size) / 2,
							ball_size, ball_size);
	mpBall->set_initial_speed();

}



//--------------------------------------------------------------
void ofApp::update(){ //Eack Frame
	old_time = new_time;
	new_time = clock() - begin_time;
	global_delta_time = int(new_time - old_time);
	//std::cout << global_delta_time << std::endl;
	//global_delta_time Calcules FPS rate

	mpPaddle1->setSpeed(0);
	mpPaddle2->setSpeed(0);
	if (key_down['w']) {
		mpPaddle1->setSpeed(-500);
	}
	if (key_down['s']) {
		mpPaddle1->setSpeed(500);
	}
	if (key_down['o']) {
		mpPaddle2->setSpeed(-500);
	}
	if (key_down['l']) {
		mpPaddle2->setSpeed(500);
	}

	mpPaddle1->update(); //Update Pad 1
	mpPaddle2->update(); //Update Pad 2
	
	//comprobar colision
	bool colPaddle1 = C_RectangleCollision(mpPaddle1->getRect(), mpBall->getRect());
	bool colPaddle2 = C_RectangleCollision(mpPaddle2->getRect(), mpBall->getRect());
	int ball_x = mpBall->getX();//pos x
	int ball_y = mpBall->getY();//pos y
	int ball_w = mpBall->getW();//ball width
	int ball_h = mpBall->getH();//ball height

	if (colPaddle1) {
		int paddle_x = mpPaddle1->getX();
		int paddle_w = mpPaddle1->getW();
		if (ball_x > paddle_x + paddle_w / 2) {
			mpBall->setX(paddle_x + paddle_w);
			mpBall->speed_augment_coll_paddle();
			mpBall->invertSpdX();
		}
		//mirar que parte toca
		//modificar SpdY segun eso
	}		
		
	if(colPaddle2) {
		int paddle_x = mpPaddle2->getX();
		int paddle_w = mpPaddle2->getW();
		if (ball_x +ball_w < paddle_x + paddle_w / 2) {
			mpBall->setX(paddle_x - ball_w);
			mpBall->speed_augment_coll_paddle();                            
			mpBall->invertSpdX();
		}
		
	}

	bool out_of_screen = false;
	//mirar si choca con bordes laterales
	if (ball_x + ball_w < 0) {//se sale por la izquierda
		//player2.score++
		out_of_screen = true;
	}
	if (ball_x > SCREEN_WIDTH) {//se sale por la derecha
		//player1.score++
		out_of_screen = true;
	}
	if (out_of_screen) {
		mpBall->setXY(	(SCREEN_WIDTH - ball_w) / 2,
						(SCREEN_HEIGHT - ball_w) / 2);
		mpBall->invertSpdX();
		mpBall->set_initial_speed();
	}
	//mirar si choca con bordes superiores
	if (ball_y < 0) {//se sale por arriba
		mpBall->invertSpdY();//invertimos Y
	}

	if (ball_y + ball_h > SCREEN_HEIGHT) {//se sale por abajo
		mpBall->invertSpdY();//invertimos Y
	}
	
	mpBall->update();	//Update Ball


}

//--------------------------------------------------------------
void ofApp::draw(){
	ofClear(0, 0, 0);

	mpPaddle1->render(); //Render Pad 1
	mpPaddle2->render(); //Render Pad 2
	mpBall->render();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){ //presionar tecla
	if (key >= 255 || key < 0) { return; }
	key_down[key] = true;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){ // soltar tecla
	if (key >= 255 || key<0) { return; }
	key_down[key] = false;
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
